# Android Coding Challenge

AjoCard takes great care in how we select members of our engineering team. This technical challenge will help us get an idea of how you would use software to solve problems for our end users.

We know that your time is important, so we’ve designed the challenge with that in mind. There's no specific amount of time that this challenge should take you to complete, as the level of effort will depend on how familiar you are with Android development and whether or not you tackle the optional functionality. Please feel free to spend as much time as you’d like crafting your preferred solution and we hope you have fun building it!

We want to know how you write code - we don't care about coding challenges where you have to reinvent the wheel by using the bare basics, we want to know how you can use the existing libraries to solve the problems that we have to solve.

## Task: Card Detail Finder 
Your task is to create a very small Android application (written in Kotlin) which will accept accept the input of the user by either of:

1. Scanning the debit / credit card number using OCR.
2. Typing a card number

and tell them the card's details inclusive of card brand, type, bank, country among others. It's up to you to decide how exactly you want to approach this challenge.

You can use the [Binlist](https://binlist.net/) API to get these details.


## What we'll look at
- Structure of the code - how you apply Clean Architecture and SOLID principles inside your code and keep it clean and reusable.
- Testing included Unit and UI tests (Please don't submit your code without any Testing!!).
- Using external APIs is cool, but you have to make sure the app will support errors if the API is down.
- A short Readme file will be appreciated just to explain your approach and explaining project parts.
- Overall user experience in the application.
- Clarity of communications in comments and other documentation
- Use of Android coding conventions
- General quality of the code and it’s resistance to crashing

## Time limit
After sending the challenge we'll wait 3 days to hear back from you. Feel free to ask us for any clarification if you need it. It's okay to leave TODOs in the code, just explan what you would still finish there if you had more time.

## Process
When you're ready, please fork this repository and start writing code in **your fork**. You'll get extra points for committing often in small chunks, so we'll see the process of how you created the application and proceed with the problem.

